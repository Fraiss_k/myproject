import json, requests

from pyramid.view import (
    view_config,
    view_defaults
    )

@view_config(route_name='home', renderer='templates/myhome.pt')
def home(request):
	return { 'name': 'Home View' }

@view_defaults(renderer='templates/mytemplate.pt')
class MyProjectViews:
	def __init__(self, request):
		self.request = request

	@view_config(route_name='news')
	def news(self):
		url = "https://api.hypem.com/v2/tracks?sort=latest&key=swagger"
		data = requests.get(url=url).json()

		return {
			'name': 'News View',
			'data': data
		}

	@view_config(route_name='popular')
	def popular(self):
		url = "https://api.hypem.com/v2/popular?key=swagger"
		data = requests.get(url=url).json()

		return {
			'name': 'Popular View',
			'data': data
		}