import unittest

from pyramid import testing

class ViewTests(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def test_home(self):
        from .views import MyProjectViews

        request = testing.DummyRequest()
        inst = MyProjectViews(request)

    #def test_plain_without_name(self):
    #    from .views import MyProjectViews

    #    request = testing.DummyRequest()
    #    inst = MyProjectViews(request)
    #    response = inst.plain()
    #    self.assertIn(b'No Name Provided', response.body)

    #def test_plain_with_name(self):
    #    from .views import MyProjectViews

    #    request = testing.DummyRequest()
    #    request.GET['name'] = 'falkn'
    #    inst = MyProjectViews(request)
    #    response = inst.plain()
    #    self.assertIn(b'falkn', response.body)

class FunctionalTests(unittest.TestCase):
    def setUp(self):
        from myproject import main
        app = main({})
        from webtest import TestApp

        self.testapp = TestApp(app)

    #def test_plain_without_name(self):
    #    res = self.testapp.get('/plain', status=200)
    #    self.assertIn(b'No Name Provided', res.body)

    #def test_plain_with_name(self):
    #    res = self.testapp.get('/plain?name=falkn', status=200)
    #    self.assertIn(b'falkn', res.body)

    def test_home(self):
        res = self.testapp.get('/howdy', status=200)
        self.assertIn(b'Home View', res.body)